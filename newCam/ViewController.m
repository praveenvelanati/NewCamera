//
//  ViewController.m
//  newCam
//
//  Created by praveen velanati on 3/30/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>


@interface ViewController ()

@end

@implementation ViewController


AVCaptureDevice *frontCamera;
AVCaptureDevice *backCamera;

@synthesize imagePreview, captureImage, stillImageOutput, cameraSwitch,btnTorch,flashBtn,segmentedSelector;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    FrontCamera = NO;
    cameraSwitch.selectedSegmentIndex = 1;
    flashBtn.hidden = true;
    segmentedSelector.hidden = true;
   
}

- (void)viewDidAppear:(BOOL)animated {
    [self initCamera];
}

- (void)initCamera {
    AVCaptureSession *session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    captureVideoPreviewLayer.frame = self.imagePreview.bounds;
    [self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
    
    UIView *view = [self imagePreview];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [imagePreview bounds];
    [captureVideoPreviewLayer setFrame:bounds];
    
    NSArray *devices = [AVCaptureDevice devices];
    //AVCaptureDevice *frontCamera;
    //AVCaptureDevice *backCamera;
    
    for (AVCaptureDevice *device in devices) {
        if ([device hasMediaType:AVMediaTypeVideo]) {
            if ([device position] == AVCaptureDevicePositionBack) {
                backCamera = device;
            }
            if ([device position] == AVCaptureDevicePositionFront) {
                frontCamera = device;
            }
        }
    }
    
    NSError *error = nil;
    if (!FrontCamera) {
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        flashBtn.hidden = false;
        [session addInput:input];
    } else {
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        flashBtn.hidden = true;
        segmentedSelector.hidden = true;
        [session addInput:input];
        
    }
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [stillImageOutput setOutputSettings:outputSettings];
    
    [session addOutput:stillImageOutput];
    [session startRunning];
}

- (IBAction)snapImage:(id)sender {
    // [capturedImage removeFromSuperview];
    [self capImage];
}

- (void) capImage {
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        
        if (videoConnection) {
            break;
        }
    }
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        if (imageSampleBuffer != NULL) {
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            UIImage *image = [UIImage imageWithData:imageData];
            self.captureImage.image = image;
          
        }
        
    }];
}


    


- (IBAction)switchCamera:(id)sender {
    if (cameraSwitch.selectedSegmentIndex == 0) {
        FrontCamera = YES;
        [self initCamera];
    } else {
        FrontCamera = NO;
        [self initCamera];
    }
}







- (IBAction)torching:(UIButton *)sender {
    
    AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if([flashLight isTorchAvailable] && [flashLight isTorchModeSupported:AVCaptureTorchModeOn])
    {
        BOOL success = [flashLight lockForConfiguration:nil];
        
        if(success) {
            
            if([flashLight isTorchActive]) {
                
                
                [btnTorch setTitle:@"TURN ON" forState:UIControlStateNormal];
                [flashLight setTorchMode:AVCaptureTorchModeOff];
                
            } else {
                
                [btnTorch setTitle:@"TURN OFF" forState:(UIControlStateNormal)];
                [flashLight setTorchMode:AVCaptureTorchModeOn];
                
                
            }
            
            
            
            [flashLight unlockForConfiguration];
            }
            
            
            
            
        }
        
        
    }

    
    

- (IBAction)flashActionButton:(UIButton *)sender {
    
    if (segmentedSelector.hidden == true){
        
        segmentedSelector.hidden = false;
    } else {
        
        segmentedSelector.hidden = true;
    }
    
    
    
}
- (IBAction)segmentActionBtn:(UISegmentedControl *)sender {
    
    
  
    
    [backCamera lockForConfiguration:nil];
    
    if (segmentedSelector.selectedSegmentIndex == 0) {
        
        if ([backCamera isFlashModeSupported:AVCaptureFlashModeOff]){
            
            [backCamera lockForConfiguration:nil];
            [backCamera setFlashMode:AVCaptureFlashModeOff];
            [backCamera unlockForConfiguration];
            
        }
    } else if (segmentedSelector.selectedSegmentIndex == 1){
        
        if([backCamera isFlashModeSupported:AVCaptureFlashModeOn]){
            
            [backCamera lockForConfiguration:nil];
            [backCamera setFlashMode:AVCaptureFlashModeOn];
            [backCamera unlockForConfiguration];
        }
        
        } else {
            
            if([backCamera isFlashModeSupported:AVCaptureFlashModeAuto]) {
            [backCamera lockForConfiguration:nil];
            [backCamera setFlashMode:AVCaptureFlashModeAuto];
            [backCamera unlockForConfiguration];

            }
            
        }
    }
    
   

    
    

        
        
        
        
        

    
    
    
    
    

@end

