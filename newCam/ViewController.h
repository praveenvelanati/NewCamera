//
//  ViewController.h
//  newCam
//
//  Created by praveen velanati on 3/30/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController


{
    
    BOOL FrontCamera;
    
}


@property (weak, nonatomic) IBOutlet UIButton *flashBtn;
- (IBAction)flashActionButton:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedSelector;

- (IBAction)segmentActionBtn:(UISegmentedControl *)sender;







@property (weak, nonatomic) IBOutlet UIButton *btnTorch;

- (IBAction)torching:(UIButton *)sender;


@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput;
@property (strong, nonatomic) IBOutlet UIView *imagePreview;
@property (strong, nonatomic) IBOutlet UIImageView *captureImage;
@property (strong, nonatomic) IBOutlet UISegmentedControl *cameraSwitch;

- (IBAction)snapImage:(id)sender;
- (IBAction)switchCamera:(id)sender;


@end

