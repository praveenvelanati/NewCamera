//
//  AppDelegate.h
//  newCam
//
//  Created by praveen velanati on 3/30/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

